import copy
import pygame
import sys
from modes import processes
from config import width_test as width, cell_size_test as cell_size, height_test as height, FPS


clock = pygame.time.Clock()
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
sc = pygame.display.set_mode((width * cell_size, height * cell_size))
present_gen = []
prev_gen = []
for i in range(height):
    line = []
    for j in range(width):
        line.append(0)
    present_gen.append(line)
pause = 1
leave = 0
while True:
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            leave = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE and not pause:
                pause = True
            elif event.key == pygame.K_SPACE and pause:
                pause = False
            elif event.key == pygame.K_DOWN and FPS > 1:
                FPS -= 1
            elif event.key == pygame.K_UP and FPS < 60:
                FPS += 1
            elif event.key == pygame.K_ESCAPE:
                leave = True
            elif event.key == pygame.K_DELETE:
                for i in present_gen:
                    for j in range(len(i)):
                        i[j] = 0
                pause = True
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1 and pause:
                cell_place_y, cell_place_x = (int(i // cell_size) for i in
                                              event.pos)  # Считываем координаты нажатой клетки
                if present_gen[cell_place_x][cell_place_y] == 1:
                    present_gen[cell_place_x][cell_place_y] = 0
                else:
                    present_gen[cell_place_x][cell_place_y] = 1
    for i in range(height):
        for j in range(width):
            if present_gen[i][j] == 0:
                pygame.draw.rect(sc, WHITE, (j * cell_size, i * cell_size, cell_size, cell_size))
            else:
                pygame.draw.rect(sc, BLACK, (j * cell_size, i * cell_size, cell_size, cell_size))
    if not pause:
        prev_gen = copy.deepcopy(present_gen)
        processes.next_generation(present_gen, height, width)
    pygame.display.update()
    if leave:
        pygame.quit()
        sys.exit()
    clock.tick(FPS)
