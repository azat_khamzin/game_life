import random
import copy
import os


def create_field(width, height):
    field = []
    for i in range(height):
        line = []
        for j in range(width):
            buff = random.randint(0, 1)
            line.append(buff)
        field.append(line)
    return field


def next_generation(pres_gen, height, width):
    lst3 = []  # Вспомогательный список для обновления поля.
    prev_gen = copy.deepcopy(pres_gen)
    for i in range(height):
        lst3.append([0] * width)
        for j in range(width):
            k = 0
            if i == height - 1:
                i = -1
            if j == width - 1:
                j = -1
            k += pres_gen[i][j - 1] + pres_gen[i - 1][j - 1] + pres_gen[i - 1][j] + pres_gen[i - 1][j + 1]
            k += pres_gen[i][j + 1] + pres_gen[i + 1][j - 1] + pres_gen[i + 1][j] + pres_gen[i + 1][j + 1]
            lst3[i][j] = k
    for i in range(height):
        for j in range(width):
            k = lst3[i][j]
            if k == 3:
                pres_gen[i][j] = 1
            elif k > 3 or k < 2:
                pres_gen[i][j] = 0
            elif k == 2:
                if prev_gen[i][j] == 0:
                    pres_gen[i][j] = 0
                else:
                    pres_gen[i][j] = 1


def check_growth(prev, pres):
    if prev == pres:
        return True
    else:
        return False


def save(gen):
    save_name = os.path.join('saves', input() + '.txt')
    f = open(save_name, 'w')
    for i in gen:
        for j in i:
            f.write(str(j) + ' ')
        f.write('\n')
    f.close()


def open_save():
    save_name = os.path.join('saves', input() + '.txt')
    file = open(save_name)
    gen = []
    for j in file:
        line = [int(i) for i in j.strip().split()]
        gen.append(line)
    return gen
