import copy
import pygame
import sys
from modes import processes
from config import width_classic as width, cell_size_classic as cell_size, height_classic as height, FPS

pygame.init()

clock = pygame.time.Clock()
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
sc = pygame.display.set_mode((width * cell_size, height * cell_size))
leave = False
present_gen = processes.create_field(width, height)
prev_gen = []
while True:
    events = pygame.event.get()
    for event in events:
        if event.type == pygame.QUIT:
            leave = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_r:
                present_gen = processes.create_field(width, height)
                prev_gen = []
            elif event.key == pygame.K_ESCAPE:
                leave = True
    for i in range(height):
        for j in range(width):
            if present_gen[i][j] == 0:
                pygame.draw.rect(sc, WHITE, (j * cell_size, i * cell_size, cell_size, cell_size))
            else:
                pygame.draw.rect(sc, BLACK, (j * cell_size, i * cell_size, cell_size, cell_size))
    prev_gen = copy.deepcopy(present_gen)
    processes.next_generation(present_gen, height, width)
    if processes.check_growth(prev_gen, present_gen):
        present_gen = processes.create_field(width, height)
        prev_gen = []
    pygame.display.update()
    if leave:
        pygame.quit()
        sys.exit()
    clock.tick(FPS)
