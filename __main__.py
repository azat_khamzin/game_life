import modes


def main():
    print("Добро пожаловать в игру 'Жизнь'")
    print('Выберите режим игры (введите номер команды):')
    print('1 - Классический     2 - Экспериментатор')
    print('3 - Творческий       4 - Закрыть программу')
    users_input = input()
    if users_input == '1':
        from modes import classic_mode
        main()
    elif users_input == '2':
        from modes import test_mode
    elif users_input == '3':
        from modes import gibrid_mode
    elif users_input == '4':
        pass
    else:
        print('Неверный код команды, повторите ввод.')
        main()


if __name__ == '__main__':
    main()
